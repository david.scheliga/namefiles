﻿namefiles
=========

.. automodule:: namefiles

   
   
   .. rubric:: Module Attributes

   .. autosummary::
   
      JsonschemaValidator
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      construct_filename
      construct_filepath
      disassemble_filename
      extract_filename_parts
      get_filename_convention
      get_filename_validator
      is_a_filename_part
      register_filename_validator
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ANameGiver
      ExtractsFilenamePart
      FilenameParts
      NameGiver
   
   

   
   
   



