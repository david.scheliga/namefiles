***************************
ANameGiver
***************************


.. autosummary::
   :toctree:

   namefiles.ANameGiver.set_parts
   namefiles.ANameGiver.to_path
   namefiles.ANameGiver.get_filename_validator
   namefiles.ANameGiver.set_name_part
   namefiles.ANameGiver.disassemble

.. autoclass:: namefiles.ANameGiver