***************************
FilenameParts
***************************

.. autosummary::
   :toctree:

   namefiles.FilenameParts.set_parts
   namefiles.FilenameParts.to_path
   namefiles.FilenameParts.get_filename_validator
   namefiles.FilenameParts.set_name_part
   namefiles.FilenameParts.disassemble
   namefiles.FilenameParts.identifier
   namefiles.FilenameParts.sub_id
   namefiles.FilenameParts.source_id
   namefiles.FilenameParts.vargroup
   namefiles.FilenameParts.context
   namefiles.FilenameParts.extension

.. autoclass:: namefiles.FilenameParts
