***************************
API reference
***************************

.. autosummary::
   :recursive:
   :toctree:

   namefiles
   namefiles.disassemble_filename
   namefiles.construct_filename
   namefiles.construct_filepath
   namefiles.extract_filename_parts
   namefiles.get_filename_convention
   namefiles.get_filename_validator
   namefiles.is_a_filename_part
   namefiles.register_filename_validator

.. toctree::

   ANameGiver/index
   FilenameParts/index
